const Course = require("../models/course");

// ================ ACTIVITY ======================

module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return error
			}

			return {
				message: 'New course successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})
		
};


// ================= ACTIVITY ====================

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};


// ================= DISCUSSION ===================

module.exports.updateCourse = (reqParams, reqBody) => {

	// specify the fields/properties of the document to be updated

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};



	/*
		Syntax:

		findByIdAndUpdate(document ID, updatesToBeApplied)
	*/

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		if (error) {

			return false;

		} else {
		
			return {
				message: "Course updated successfully"
			}
		};
	
	});	

};

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {

		isActive: false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return {
		
				message: "Archived Course successfully"
			}
		};
	});
};



module.exports.unarchiveCourse = (reqParams) => {

	let updateActiveField = {

		isActive: true
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return {
		
				message: "Unarchived Course successfully"
			}
		};
	});
};


	// Mini Activity
	/*
		Make a query on the course model that retrieves all the active products using find method.
	*/


module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result;
	});
};










