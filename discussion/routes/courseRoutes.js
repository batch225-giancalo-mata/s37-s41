const express = require("express");
const router = express.Router();
const auth = require("../auth");
const courseController = require("../controllers/courseController");

// Router for creating a course

router.post("/addCourse", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result));
})



// ACTIVITY

// controller function for retrieving all the courses

router.get("/getAllCourses", (req, res) => {

	courseController.getAllCourses().then(
		resultFromController => res.send(resultFromController));
});


// Route for updating the course

router.put("/updateCourse/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});



// Routes for Archiving

// Using Params
router.put("/archive/:courseId", auth.verify, (req,res) => {

	courseController.archiveCourse(req.params).then(result => res.send(result));

});



router.put("/unarchive/:courseId", auth.verify, (req,res) => {

	courseController.unarchiveCourse(req.params).then(result => res.send(result));

});




// Router for users in getting courses - users only

router.get("/getAllActive", auth.verify, (req, res) => {

	courseController.getAllActive().then(result => res.send(result));
});


/*
	Plan for capstone:

	Admins - Manage All - (Product)

	Users - Buy Active Only - (Product)

*/

module.exports = router;